<?php
namespace CryptoUrlTransmission;

/**
 * Class that handles a simple Authenticated Encryption using
 * Encrypt-then-MAC (EtM) 
 * 
 * A mcrypt wrapper
 * 
 * Based on
 * http://php.net/manual/es/function.mcrypt-encrypt.php#116612
 *
 * @author Julio González <ontananza@msn.com>
 * 
 * @version 1.0
 *
 */
class Crypt {
    
    /**
     * Max key length to use.
     *
     * @var number
     */
    private $key_length=0;
    
    /**
     * Generated errors
     *
     * @var array $errors
     */
    private $errors=array();
    
    /**
     * Defined salt
     *
     * @var string
     */
    private $salt = 'Oomy56J789A#4z34';
    
    /**
     * By now only sha256 to validate integrity
     *
     * @var string
     */
    private $mac_algorithm = 'sha256';
    
    /**
     * Object configuration
     *
     * @var array
     */
    private $_config = array(
        'key'=>'',
        'enc_algorithm'=> MCRYPT_RIJNDAEL_256,
        'enc_mode'=> MCRYPT_MODE_CBC,
    );
    
    /**
     * Save object configuration
     *
     * @param array $config
     */
    public function config($config=array()){
        
        foreach ($this->_config as $key=>$val){
            
            if (isset($config[$key])){
                $this->_config[$key]=$config[$key];
            }
        }
        
        $this->key_length = mcrypt_get_key_size($this->_config['enc_algorithm'], $this->_config['enc_mode']);
    }
    
    /**
     * Check if string is a base64 encoded string.
     * 
     * @param string $string
     * @return boolean
     */
    private function isBase64($string){
        return (bool) preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $string);
    }
    
    /**
     * Generates a key of the maximum length supported by the encryption algorithm
     * 
     * @return int
     */
    private function compliantKey(){
        //-uniqness to salt???
        $tmp_salt = md5($this->salt.$this->_config['key']);
        return $this->pbkdf2($this->mac_algorithm,$this->_config['key'],$tmp_salt,2*$this->key_length,$this->key_length,true);
    }
    
    /*
     * PBKDF2 key derivation function as defined by RSA's PKCS #5: https://www.ietf.org/rfc/rfc2898.txt
     * $algorithm - The hash algorithm to use. Recommended: SHA256
     * $password - The password.
     * $salt - A salt that is unique to the password.
     * $count - Iteration count. Higher is better, but slower. Recommended: At least 1024.
     * $key_length - The length of the derived key in bytes.
     * $raw_output - If true, the key is returned in raw binary format. Hex encoded otherwise.
     * Returns: A $key_length-byte key derived from the password and salt.
     *
     * Test vectors can be found here: https://www.ietf.org/rfc/rfc6070.txt
     *
     * This implementation of PBKDF2 was originally created by defuse.ca
     * With improvements by variations-of-shadow.com
     *
     * Based on
     * http://php.net/manual/en/function.hash-hmac.php#109260
     *
     */
    private function pbkdf2($algorithm = 'sha256', $password='', $salt, $count, $key_length, $raw_output = false){
        
        $flag0 = true;
        $datos = false;
        
        $algorithm = strtolower($algorithm);
        
        if(!in_array($algorithm, hash_algos(), true)){
            $flag0=false;
            $this->errors[]= 'PBKDF2 ERROR: Invalid hash algorithm.';
        }
        
        if($count <= 0 || $key_length <= 0){
            $flag0=false;
            $this->errors[]= 'PBKDF2 ERROR: Invalid parameters.';
        }
        
        if ($flag0){
            
            $hash_length = strlen(hash($algorithm, "", true));
            $block_count = ceil($key_length / $hash_length);
            
            $output = "";
            for($i = 1; $i <= $block_count; $i++) {
                // $i encoded as 4 bytes, big endian.
                $last = $salt . pack("N", $i);
                // first iteration
                $last = $xorsum = hash_hmac($algorithm, $last, $password, true);
                // perform the other $count - 1 iterations
                for ($j = 1; $j < $count; $j++) {
                    $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
                }
                $output .= $xorsum;
            }
            
            $datos = $raw_output ? substr($output, 0, $key_length) : bin2hex(substr($output, 0, $key_length));
        }
        
        return $datos;
    }
    
    /**
     * Class Builder
     *
     * @param array $config
     */
    public function __construct($config=array()){
        $this->config($config);
    }
    
    /**
     * Returns the block size of the mac
     *
     * @param string $algorithm
     */
    private function getMacAlgoBlockSize($algorithm = 'sha256') {
        
        $length = false;
        
        switch($algorithm){
            
            case 'sha1':
                $length=160;
                break;
                
            case 'sha256':
                $length=256;
                break;
        }
        
        return $length;
    }
    
    /**
     * Decodes a message
     *
     * @param string $message
     * @return mixed
     */
    public function decrypt($message){
        
        $key = $this->compliantKey();
        
        $message = $this->isBase64($message) ? base64_decode($message) : "";
        $message_decrypted = false;
        
        //-si se decodificó
        if (is_string($message) && $message!=""){
            
            $iv_size = mcrypt_get_iv_size($this->_config['enc_algorithm'], $this->_config['enc_mode']);
            $mac_block_size = (int)ceil($this->getMacAlgoBlockSize($this->mac_algorithm)/8);
            
            $mac = substr($message, strlen($message) - $mac_block_size);
            
            $message= substr($message, 0, strlen($message) - $mac_block_size);
            
            $mac_dec = $this->getMac($message, $key);
            
            $iv_dec = substr($message, 0, $iv_size);
            $message= substr($message, $iv_size);
                        
            $message_decrypted= mcrypt_decrypt($this->_config['enc_algorithm'], $key, $message, $this->_config['enc_mode'], $iv_dec);

            $message_decrypted = rtrim($message_decrypted, "\0");
            
            //-checando integridad!
            if (!self::hashCompare($mac, $mac_dec)){
                $this->errors[]="Bad integrity check!!!";
            }
            
        }else {
            $this->errors[]="Empty message or bad base64 decode";
        }
        
        return empty($this->errors) ? $message_decrypted : false;
    }
     
    /**
     * Encodes a message
     *
     * @param string $message
     * @return string
     */
    public function encrypt($message){
        
        $key = $this->compliantKey();
        
        $iv_size = mcrypt_get_iv_size($this->_config['enc_algorithm'], $this->_config['enc_mode']);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        
        $ciphertext = mcrypt_encrypt($this->_config['enc_algorithm'], $key, $message, $this->_config['enc_mode'], $iv);
        
        $mac = $this->getMac($iv.$ciphertext, $key);
        
        return empty($this->errors) ? base64_encode($iv . $ciphertext . $mac) : false;
    }
    
    /**
     * Returns the mac of a message with a given key
     * 
     * @param string $message
     * @param string $key
     */
    public function getMac($message, $key){
        
        $mac = hash_hmac($this->mac_algorithm, $message, $key, true);
        $mac_block_size = (int)ceil($this->getMacAlgoBlockSize($this->mac_algorithm)/8);
        return substr($mac, 0, $mac_block_size);
    }
    
    /**
     * Returns the errors produced
     *
     * @return array
     */
    public function getErrors(){
        return $this->errors;
    }
    
    /**
     * Compare two strings char by char to the last one. To avoid timing attacks
     * http://php.net/manual/es/function.hash-hmac.php
     *
     * @param string $a
     * @param string $b
     *
     * @return boolean
     */
    public static function hashCompare($a, $b) {
        
        if (!is_string($a) || !is_string($b)) {
            return false;
        }
        
        $len = strlen($a);
        if ($len !== strlen($b)) {
            return false;
        }
        
        $status = 0;
        for ($i = 0; $i < $len; $i++) {
            $status |= ord($a[$i]) ^ ord($b[$i]);
        }
        
        return $status === 0;
    }
}