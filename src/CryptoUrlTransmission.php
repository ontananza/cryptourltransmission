<?php
namespace CryptoUrlTransmission;

/**
 * Class that handles the generation and validation of data tokens
 * depends on Crypt.class
 *
 * @author Julio González <ontananza@msn.com>
 * 
 * @version 1.0
 *
 */

class CryptoUrlTransmission {
   
    /**
     * Data enconding options...
     * 
     * @var array
     */
    private $encode_options = array(
        'json','serialize',
    );
    
    /**
     * Encodings list to detect...
     *
     * @var string
     */
    private static $encoding_list="UTF-8, Windows-1252, ASCII, ISO-8859-1, ISO-8859-15";
    
    /**
     * Data of the token...
     * 
     * @var array
     */
    private $data = array();
    
    /**
     * Errors
     *
     * @var array
     */
    private $errors = array();
    
    /**
     * Object config
     *
     * @var array
     */
    private $_config = array(
        'encode'=>'json',
        'gzip'=>true,
        'shared_key'=>'',
        'shuffle'=>false,
    );
    
    /**
     * Crypt object
     * 
     * @var Crypt
     */
    private $crypt;
    
    /**
     * Save object configuration
     *
     * @param array $config
     */
    public function config($config=array()){
        
        foreach ($this->_config as $key=>$val){
            
            if (isset($config[$key])){
                $this->_config[$key]=$config[$key];
            }
        }
    }
    
    /**
     * Constructor of the class...
     * 
     * @param mixed $data
     * @param array $config
     */
    public function __construct($data, $config=array()){
        
        $this->config($config);
        
        if ($this->_config['shared_key'] != ''){
            
            $this->crypt = new \CryptoUrlTransmission\Crypt(array(
                'key'=>$this->_config['shared_key'],
            ));
        }else {
            $this->errors[]='Need a shared key string not empty';
        }
        
        if (is_string($data)){	
            
            $this->getDataFromString($data);
            
        }elseif (is_array($data)){ 
            
            $this->data = is_array($data)?$data:array();
        }
        
    }
    
    /**
     * Get data of the token...
     *
     * @return mixed
     */
    public function getData(){
        
        return $this->data;
    }
    
    /**
     * GEt errors...
     *
     * @return array
     */
    public function getErrors(){
        return $this->errors;
    }
    
    /**
     * Randomizes the keys of 
     * array preserving the keys
     *
     * @param array $array
     * @return array
     */
    private static function shuffle($array){
        
        $keys = array_keys($array);
        shuffle($keys);
        $data = array();
        
        foreach ($keys as $key){
            $data[$key] = $array[$key];
        }
        
        return $data;
    }
    
    /**
     * Returns a string in the selected encoding, detecting the original
     * Accepts the encodings defineds on self::$encoding_list
     *
     * @param string $string
     * @param string $encoding
     * @return string
     */
    private static function encodeTo($string,$encoding='UTF-8'){
        return mb_convert_encoding($string, $encoding, mb_detect_encoding($string, self::$encoding_list, true));
    }
    
    /**
     * Getting the data of token from string...
     *
     * @param string $string
     */
    private function getDataFromString($string=''){
        
        $data = false;
        
        $string = is_object($this->crypt) ? $this->crypt->decrypt($string) : false;
        
        if (is_string($string)){
            
            $string = $this->_config['gzip'] ? @gzinflate($string) : $string;
            $string = self::encodeTo(trim($string));
            
            $data = $this->_config['encode'] == 'json' ? @json_decode($string,true) : @unserialize($string);
        }else if (is_object($this->crypt)){
            $this->errors = array_merge($this->errors,$this->crypt->getErrors());
        }else {
            $this->errors[] = 'Decrypt message wasnt possible.';
        }
        
        $this->data = $data;
    }
    
    /**
     * Adds or replaces the data array,
     * With a string or an array of data.
     *
     * @param mixed $data
     * @param boolean $append
     */
    public function setData($data=array(),$append=true){
        
        if ($append && is_array($data)){
            
            $this->data = $this->data+$data;
            
        }elseif (!$append && is_array($data)){
            
            $this->data =$data;
        }elseif (is_string($data)){
            
            $this->getDataFromString($data);
        }
    }
    
    /**
     * Regresa la representacion en cadena de los datos de Un token.
     *
     * @return string
     */
    public function getString(){
        
        $data = $this->data;
        
        $tmp_array = $this->_config['shuffle'] ? self::shuffle($data) : $data;
        
        $string_tmp_array = $this->_config['encode'] == 'json' ? json_encode($tmp_array) : serialize($tmp_array);
        $string_tmp_array = $this->_config['gzip'] ? gzdeflate($string_tmp_array) : $string_tmp_array;
        
        if (is_object($this->crypt)){
            $string_tmp_array = $this->crypt->encrypt($string_tmp_array);
        }else {
            $string_tmp_array = '';
            $this->errors[]='Encrypt message was not possible.';
        }
        
        return $string_tmp_array;
    }
}