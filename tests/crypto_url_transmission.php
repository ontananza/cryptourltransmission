<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
require_once '../src/Crypt.php';
require_once '../src/CryptoUrlTransmission.php';

$shared_key = '23Wdg.Y,';
$gzip = true;

$data = array(
  'key1'=>0,  
  'key2'=>'Cadena con algún dato a mandar!!!!',  
  'key3'=>'Cadena con algún dato a mandar!!!!',  
  'key4'=>array(
      'banana','naranja','limón'
  ),
    'key5'=>pi(),
    'key6'=>pi()+1,
    'key7'=>pi()+2,
);


$token = new \CryptoUrlTransmission\CryptoUrlTransmission($data, array(
    'shared_key'=>$shared_key,
    'gzip'=>$gzip,
));

/*$token->setData(array(
    'otro'=>'Hola mundo'
));*/

echo '<pre>';

$string_token = $token->getString();
echo 'Data to be crypted'.PHP_EOL;

print_r($data);

echo 'Token: '.$string_token.PHP_EOL;
echo 'Token length: '.strlen($string_token).PHP_EOL;


$token2 = new \CryptoUrlTransmission\CryptoUrlTransmission($string_token, array(
    'shared_key'=>$shared_key,
    'gzip'=>$gzip,
));

echo 'Data obtained of Token: '.PHP_EOL;

print_r($token2->getData());

echo 'Errors obtained of Token: '.PHP_EOL;

print_r($token2->getErrors());