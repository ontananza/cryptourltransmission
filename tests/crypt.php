<?php 


require_once '../src/Crypt.php';

$key = '43We667!.56x01Z';

$crypto = new \CryptoUrlTransmission\Crypt([
    'key'=>$key,
]);

echo '<pre>';

$message = 'Árbol, pingüino. 
Este es mi mensaje a transmitir!!! 01234567890
ZZZ.
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.';

//$message = 'Hola a todos x';

$message_encrypted = $crypto->encrypt($message);

$message_decrypted = $crypto->decrypt($message_encrypted);

echo 'El mensaje original'.PHP_EOL;

echo var_dump($message).PHP_EOL;


echo 'El mensaje encriptado'.PHP_EOL;

echo var_dump($message_encrypted).PHP_EOL;

echo 'El mensaje des encriptado'.PHP_EOL;

echo var_dump($message_decrypted).PHP_EOL;


